#!jinja|yaml

{% set profile = {
    'aws_secret_key_id': None,
    'client_id': 'CLIENT_ID',
    'secret': 'SECRET',
    'subscription_id': 'SUBSCRIPTION_ID',
    'tenant': 'TENANT' }
    %}
# From the top level directory, run with
# idem -T example --sls disk
Ensure/establish an EBS volume exists:
  botoaws.disk.volume.present:
    - name: pcn-test-disk-01
    - size: 100  # 100gb?
    - az: us-east-1a
    - region: us-east-1
    - tags:
        contact_name: Elmer Fudd Gantry
