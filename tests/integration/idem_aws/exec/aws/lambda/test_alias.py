import copy
import pytest


@pytest.mark.asyncio
async def test_create(hub, ctx, instance_name, aws_lambda_function):
    status, result = await hub.exec.aws.λ.alias.create(
        ctx,
        instance_name,
        function_name=aws_lambda_function.name,
        function_version=aws_lambda_function.configuration.version,
    )
    assert status, result
    copy.deepcopy(result)


@pytest.mark.asyncio
async def test_list(hub, ctx, instance_name, aws_lambda_function):
    status, result = await hub.exec.aws.λ.alias.list(ctx, aws_lambda_function.name)
    assert status, result
    assert instance_name in result
    copy.deepcopy(result)


@pytest.mark.asyncio
async def test_get(hub, ctx, instance_name, aws_lambda_function):
    status, result = await hub.exec.aws.λ.alias.get(
        ctx, instance_name, aws_lambda_function.name
    )
    assert status, result
    assert result.name == instance_name
    assert result.tags == {ctx.acct.provider_tag_key: instance_name}
    copy.deepcopy(result)


@pytest.mark.asyncio
async def test_tag(hub, ctx, instance_name, aws_lambda_function):
    status, result = await hub.exec.aws.λ.alias.tag(
        ctx,
        instance_name,
        aws_lambda_function.name,
        tags={"idem_aws_test_tag": "value"},
    )
    assert status, result
    copy.deepcopy(result)


@pytest.mark.asyncio
async def test_tags(hub, ctx, instance_name, aws_lambda_function):
    status, result = await hub.exec.aws.λ.alias.tags(
        ctx, instance_name, aws_lambda_function.name
    )
    assert status, result
    assert result == {
        ctx.acct.provider_tag_key: instance_name,
        "idem_aws_test_tag": "value",
    }
    copy.deepcopy(result)


@pytest.mark.asyncio
async def test_untag(hub, ctx, instance_name, aws_lambda_function):
    status, result = await hub.exec.aws.λ.alias.untag(
        ctx, instance_name, aws_lambda_function.name, keys=["idem_aws_test_tag"]
    )
    assert status, result
    copy.deepcopy(result)

    # Verify that the tag is now gone
    status, result = await hub.exec.aws.λ.alias.tags(
        ctx, instance_name, aws_lambda_function.name
    )
    assert status, result
    assert result == {ctx.acct.provider_tag_key: instance_name}


@pytest.mark.asyncio
async def test_update(hub, ctx, instance_name, aws_lambda_function):
    status, result = await hub.exec.aws.λ.alias.update(
        ctx, instance_name, aws_lambda_function.name, description="New description!"
    )
    assert status, result
    copy.deepcopy(result)


@pytest.mark.asyncio
@pytest.mark.skip("Method not allowed")
async def test_delete(hub, ctx, instance_name, aws_lambda_function):
    status, result = await hub.exec.aws.λ.alias.delete(
        ctx, instance_name, aws_lambda_function.name
    )
    # TODO it doesn't look like aliases are being delete properly with localstack, but they get cleaned
    #  up for the test when the function is deleted in the teardown
    assert status, result
    copy.deepcopy(result)
