import copy
import pytest


@pytest.mark.asyncio
async def test_create(hub, ctx, instance_name, aws_iam_role, zip_file):
    status, role = await hub.exec.aws.iam.role.get(ctx, aws_iam_role.name)
    assert status, role.exception
    status, result = await hub.exec.aws.λ.function.create(
        ctx,
        name=instance_name,
        runtime="python3.7",
        role=role.arn,
        handler="code.main",
        code={"ZipFile": zip_file},
    )
    assert status, result
    copy.deepcopy(result)


@pytest.mark.asyncio
async def test_get(hub, ctx, instance_name):
    status, result = await hub.exec.aws.λ.function.get(ctx, instance_name)
    assert status, result
    assert result.name == instance_name
    assert result.tags == {ctx.acct.provider_tag_key: instance_name}


@pytest.mark.asyncio
async def test_list(hub, ctx, instance_name):
    status, result = await hub.exec.aws.λ.function.list(ctx)
    assert status, result
    assert instance_name in result
    copy.deepcopy(result)


@pytest.mark.asyncio
async def test_tag(hub, ctx, instance_name):
    status, result = await hub.exec.aws.λ.function.tag(
        ctx, instance_name, tags={"idem_aws_test_tag": "value"},
    )
    assert status, result
    copy.deepcopy(result)


@pytest.mark.asyncio
async def test_tags(hub, ctx, instance_name):
    status, result = await hub.exec.aws.λ.function.tags(ctx, instance_name)
    assert status, result
    assert result == {
        ctx.acct.provider_tag_key: instance_name,
        "idem_aws_test_tag": "value",
    }
    copy.deepcopy(result)


@pytest.mark.asyncio
async def test_untag(hub, ctx, instance_name):
    status, result = await hub.exec.aws.λ.function.untag(
        ctx, instance_name, keys=["idem_aws_test_tag"]
    )
    assert status, result
    copy.deepcopy(result)

    # Verify that the tag is now gone
    status, result = await hub.exec.aws.λ.function.tags(ctx, instance_name)
    assert status, result
    assert result == {ctx.acct.provider_tag_key: instance_name}


@pytest.mark.asyncio
async def test_update_code(hub, ctx, instance_name, zip_file):
    status, result = await hub.exec.aws.λ.function.update(
        ctx, "code", instance_name, zip_file=zip_file
    )
    assert status, result
    copy.deepcopy(result)


@pytest.mark.asyncio
async def test_update_configuration(hub, ctx, instance_name):
    status, result = await hub.exec.aws.λ.function.update(
        ctx, "configuration", instance_name, timeout=100
    )
    assert status, result
    copy.deepcopy(result)


@pytest.mark.asyncio
async def test_delete(hub, ctx, instance_name):
    status, result = await hub.exec.aws.λ.function.delete(ctx, instance_name)
    assert status, result
    copy.deepcopy(result)
