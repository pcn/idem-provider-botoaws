import pytest
import copy


@pytest.mark.asyncio
async def test_create(hub, ctx, instance_name):
    status, result = await hub.exec.aws.s3.bucket.create(ctx, instance_name)
    assert status, result
    copy.deepcopy(result)


@pytest.mark.asyncio
async def test_list(hub, ctx, instance_name):
    status, result = await hub.exec.aws.s3.bucket.list(ctx)
    assert status, result
    assert instance_name in result
    copy.deepcopy(result)


@pytest.mark.asyncio
async def test_get(hub, ctx, instance_name):
    status, result = await hub.exec.aws.s3.bucket.get(ctx, instance_name)
    assert status, result
    assert result.name == instance_name
    assert result.tags == {ctx.acct.provider_tag_key: instance_name}
    copy.deepcopy(result)


@pytest.mark.asyncio
async def test_tag(hub, ctx, instance_name):
    status, result = await hub.exec.aws.s3.bucket.tag(
        ctx, instance_name, tags={"idem_aws_test_tag": "value"}
    )
    assert status, result
    copy.deepcopy(result)


@pytest.mark.asyncio
async def test_tags(hub, ctx, instance_name):
    status, result = await hub.exec.aws.s3.bucket.tags(ctx, instance_name)
    assert status, result
    assert result == {
        ctx.acct.provider_tag_key: instance_name,
        "idem_aws_test_tag": "value",
    }
    copy.deepcopy(result)


@pytest.mark.asyncio
async def test_delete(hub, ctx, instance_name):
    status, result = await hub.exec.aws.s3.bucket.delete(ctx, instance_name)
    assert status, result

    _, buckets = await hub.exec.aws.s3.bucket.list(ctx)
    assert instance_name not in buckets
    copy.deepcopy(result)
