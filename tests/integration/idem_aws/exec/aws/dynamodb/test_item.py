import copy
import pytest


@pytest.mark.asyncio
async def test_create(hub, ctx, aws_dynamodb_table, aws_dynamodb_item_name):
    status, result = await hub.exec.aws.dynamodb.item.create(
        ctx,
        name=aws_dynamodb_item_name,
        table=aws_dynamodb_table.name,
        data={"S": "value"},
    )
    assert status, result
    copy.deepcopy(result)


@pytest.mark.asyncio
async def test_get(hub, ctx, aws_dynamodb_table, aws_dynamodb_item_name):
    status, result = await hub.exec.aws.dynamodb.item.get(
        ctx, aws_dynamodb_item_name, table=aws_dynamodb_table.name
    )
    assert status, result
    copy.deepcopy(result)


@pytest.mark.asyncio
async def test_list(hub, ctx, aws_dynamodb_table, aws_dynamodb_item_name):
    status, result = await hub.exec.aws.dynamodb.item.list(
        ctx, table=aws_dynamodb_table.name
    )
    assert status, result
    assert aws_dynamodb_item_name in result
    copy.deepcopy(result)


@pytest.mark.asyncio
async def test_update(hub, ctx, aws_dynamodb_table, aws_dynamodb_item_name):
    status, result = await hub.exec.aws.dynamodb.item.update(
        ctx,
        aws_dynamodb_item_name,
        table=aws_dynamodb_table.name,
        data={"S": "string"},
        return_values="NONE",
    )
    assert status, result
    copy.deepcopy(result)


@pytest.mark.asyncio
async def test_delete(hub, ctx, aws_dynamodb_table, aws_dynamodb_item_name):
    status, result = await hub.exec.aws.dynamodb.item.delete(
        ctx,
        name=aws_dynamodb_item_name,
        table=aws_dynamodb_table.name,
        data={"S": "string"},
    )
    assert status, result
    copy.deepcopy(result)
