import copy
import pytest


@pytest.mark.asyncio
async def test_create(hub, ctx, instance_name):
    status, result = await hub.exec.aws.dynamodb.global_table.create(ctx, instance_name)
    assert status, result
    copy.deepcopy(result)


@pytest.mark.asyncio
async def test_get(hub, ctx, instance_name):
    status, result = await hub.exec.aws.dynamodb.global_table.get(ctx, instance_name)
    assert status, "Table does not exist"
    copy.deepcopy(result)


@pytest.mark.asyncio
async def test_list(hub, ctx, instance_name):
    status, result = await hub.exec.aws.dynamodb.global_table.list(ctx)
    assert status, result
    assert instance_name in result
    copy.deepcopy(result)


@pytest.mark.asyncio
async def test_delete(hub, ctx, instance_name):
    status, result = await hub.exec.aws.dynamodb.global_table.delete(ctx, instance_name)
    assert status, result
    copy.deepcopy(result)
