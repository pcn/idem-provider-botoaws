import copy
import pytest


@pytest.mark.asyncio
async def test_create(hub, ctx, instance_name, aws_dynamodb_item_name):
    status, result = await hub.exec.aws.dynamodb.table.create(
        ctx,
        name=instance_name,
        # FIXME?  Should we have this be {'string': 'S'} and then have the exec module turn it into this vv
        attribute_definitions=[
            {"AttributeName": aws_dynamodb_item_name, "AttributeType": "S"}
        ],
        # FIXME?  Should we have this be {'string': 'HASH'} and then have the exec module turn it into this vv
        key_schema=[{"AttributeName": aws_dynamodb_item_name, "KeyType": "HASH"}],
        # FIXME? Should ReadCapacityUnits and WriteCapacityUnits be their own arguments and then have the exec module turn them into this? vv
        provisioned_throughput={"ReadCapacityUnits": 123, "WriteCapacityUnits": 123},
        # FIXME? Should there be a global contract that always turns sane tag dictionaries into this? vv
        tags=[{"Key": aws_dynamodb_item_name, "Value": aws_dynamodb_item_name}],
    )
    assert status, result
    copy.deepcopy(result)


@pytest.mark.asyncio
async def test_get(hub, ctx, instance_name, aws_dynamodb_item_name):
    status, result = await hub.exec.aws.dynamodb.table.get(ctx, instance_name)
    assert status, "Table does not exist"
    # Verify all the tags
    assert result.tags == {
        ctx.acct.provider_tag_key: instance_name,
        aws_dynamodb_item_name: aws_dynamodb_item_name,
    }
    copy.deepcopy(result)


@pytest.mark.asyncio
async def test_list(hub, ctx, instance_name):
    status, result = await hub.exec.aws.dynamodb.table.list(ctx)
    assert status, result
    assert instance_name in result
    copy.deepcopy(result)


@pytest.mark.asyncio
async def test_tag(hub, ctx, instance_name):
    status, result = await hub.exec.aws.dynamodb.table.tag(
        ctx, instance_name, tags={"idem_aws_test_tag": "value"}
    )
    assert status, result
    copy.deepcopy(result)


@pytest.mark.asyncio
async def test_tags(hub, ctx, instance_name, aws_dynamodb_item_name):
    status, result = await hub.exec.aws.dynamodb.table.tags(ctx, instance_name)
    assert status, result
    assert result == {
        ctx.acct.provider_tag_key: instance_name,
        "idem_aws_test_tag": "value",
        aws_dynamodb_item_name: aws_dynamodb_item_name,
    }
    copy.deepcopy(result)


@pytest.mark.asyncio
async def test_untag(hub, ctx, instance_name, aws_dynamodb_item_name):
    status, result = await hub.exec.aws.dynamodb.table.untag(
        ctx, instance_name, keys=["idem_aws_test_tag"]
    )
    assert status, result
    copy.deepcopy(result)

    # Verify that the tag is now gone
    status, result = await hub.exec.aws.dynamodb.table.tags(ctx, instance_name)
    assert status, result
    assert result == {
        ctx.acct.provider_tag_key: instance_name,
        aws_dynamodb_item_name: aws_dynamodb_item_name,
    }


@pytest.mark.asyncio
async def test_update(hub, ctx, instance_name):
    status, result = await hub.exec.aws.dynamodb.table.update(
        ctx, instance_name, billing_mode="PROVISIONED"
    )
    assert status, result
    copy.deepcopy(result)


@pytest.mark.asyncio
async def test_delete(hub, ctx, instance_name):
    status, result = await hub.exec.aws.dynamodb.table.delete(ctx, instance_name)
    assert status, result
    copy.deepcopy(result)
