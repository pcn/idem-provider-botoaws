import copy
import pytest


@pytest.mark.asyncio
async def test_create(hub, ctx, instance_name):
    status, result = await hub.exec.aws.ec2.volume.create(
        ctx, instance_name, availability_zone="zone", size=1024,
    )
    assert status, result
    copy.deepcopy(result)


@pytest.mark.asyncio
@pytest.mark.skip(
    "Invalid literal for int... idk, a nearly identical operation works for 'get'"
)
async def test_list(hub, ctx, instance_name):
    status, result = await hub.exec.aws.ec2.volume.list(ctx)
    assert status, result
    assert instance_name in result
    copy.deepcopy(result)


@pytest.mark.asyncio
async def test_get(hub, ctx, instance_name):
    status, result = await hub.exec.aws.ec2.volume.get(ctx, instance_name)
    assert status, result
    assert result.name == instance_name
    assert result.tags == {ctx.acct.provider_tag_key: instance_name}
    copy.deepcopy(result)


@pytest.mark.asyncio
async def test_tag(hub, ctx, instance_name):
    status, result = await hub.exec.aws.ec2.volume.tag(
        ctx, instance_name, tags={"idem_aws_test_tag": "value"}
    )
    assert status, result
    copy.deepcopy(result)


@pytest.mark.asyncio
async def test_tags(hub, ctx, instance_name):
    status, result = await hub.exec.aws.ec2.volume.tags(ctx, instance_name)
    assert status, result
    assert result == {
        ctx.acct.provider_tag_key: instance_name,
        "idem_aws_test_tag": "value",
    }
    copy.deepcopy(result)


@pytest.mark.asyncio
async def test_untag(hub, ctx, instance_name):
    status, result = await hub.exec.aws.ec2.volume.untag(
        ctx, instance_name, keys=["idem_aws_test_tag"]
    )
    assert status, result
    copy.deepcopy(result)

    # Verify that the tag is now gone
    status, result = await hub.exec.aws.ec2.volume.tags(ctx, instance_name)
    assert status, result
    assert result == {ctx.acct.provider_tag_key: instance_name}


@pytest.mark.asyncio
@pytest.mark.skip("Localstack doesn't handle this very well")
async def test_update(hub, ctx, instance_name):
    status, result = await hub.exec.aws.ec2.volume.update(
        ctx, "modify_attribute", instance_name, AutoEnableIO={"Value": True}
    )
    assert status, result
    copy.deepcopy(result)


@pytest.mark.asyncio
async def test_delete(hub, ctx, instance_name):
    status, result = await hub.exec.aws.ec2.volume.delete(ctx, instance_name)
    assert status, result
    copy.deepcopy(result)
