import copy
import pytest


@pytest.mark.asyncio
async def test_create(hub, ctx, instance_name, aws_ec2_subnet):
    status, result = await hub.exec.aws.ec2.instance.create(
        ctx, name=instance_name, subnet=aws_ec2_subnet.name
    )
    assert status, result
    copy.deepcopy(result)


@pytest.mark.asyncio
async def test_list(hub, ctx, instance_name):
    status, result = await hub.exec.aws.ec2.instance.list(ctx)
    assert status, result
    assert instance_name in result
    copy.deepcopy(result)


@pytest.mark.asyncio
async def test_get(hub, ctx, instance_name, aws_ec2_subnet, aws_ec2_vpc):
    status, result = await hub.exec.aws.ec2.instance.get(ctx, instance_name)
    assert status, result
    assert result.name == instance_name
    assert result.tags == {ctx.acct.provider_tag_key: instance_name}
    assert result.subnet_id == aws_ec2_subnet.subnet_id
    assert result.vpc_id == aws_ec2_vpc.vpc_id
    copy.deepcopy(result)


@pytest.mark.asyncio
async def test_tag(hub, ctx, instance_name):
    _, result = await hub.exec.aws.ec2.instance.tag(
        ctx, instance_name, tags={"idem_aws_test_tag": "value"}
    )
    assert result
    copy.deepcopy(result)


@pytest.mark.asyncio
async def test_tags(hub, ctx, instance_name):
    status, result = await hub.exec.aws.ec2.instance.tags(ctx, instance_name)
    assert status, result
    assert result == {
        ctx.acct.provider_tag_key: instance_name,
        "idem_aws_test_tag": "value",
    }
    copy.deepcopy(result)


@pytest.mark.asyncio
async def test_untag(hub, ctx, instance_name):
    status, result = await hub.exec.aws.ec2.instance.untag(
        ctx, instance_name, keys=["idem_aws_test_tag"]
    )
    assert status, result
    copy.deepcopy(result)

    # Verify that the tag is now gone
    status, result = await hub.exec.aws.ec2.instance.tags(ctx, instance_name)
    assert status, result
    assert result == {ctx.acct.provider_tag_key: instance_name}


@pytest.mark.asyncio
async def test_update(hub, ctx, instance_name):
    status, result = await hub.exec.aws.ec2.instance.update(
        ctx, action="stop", name=instance_name
    )
    assert status, result
    copy.deepcopy(result)


@pytest.mark.asyncio
async def test_delete(hub, ctx, instance_name):
    status, result = await hub.exec.aws.ec2.instance.delete(ctx, instance_name)
    assert status, result
    copy.deepcopy(result)
