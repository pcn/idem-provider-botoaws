import copy
import pytest


@pytest.mark.asyncio
async def test_create(hub, ctx, instance_name):
    status, result = await hub.exec.aws.iam.role.create(
        ctx, instance_name, assume_role_policy_document="string"
    )
    assert status, result
    copy.deepcopy(result)


@pytest.mark.asyncio
async def test_list(hub, ctx, instance_name):
    status, result = await hub.exec.aws.iam.role.list(ctx)
    assert status, result
    assert instance_name in result
    copy.deepcopy(result)


@pytest.mark.asyncio
async def test_get(hub, ctx, instance_name):
    status, result = await hub.exec.aws.iam.role.get(ctx, instance_name)
    assert status, result
    assert result.name == instance_name
    assert result.tags == {ctx.acct.provider_tag_key: instance_name}
    copy.deepcopy(result)


@pytest.mark.asyncio
async def test_delete(hub, ctx, instance_name):
    status, result = await hub.exec.aws.iam.role.delete(ctx, instance_name)
    assert status, result
    copy.deepcopy(result)
