import copy
import pytest


@pytest.mark.asyncio
async def test_create(hub, ctx, instance_name):
    status, result = await hub.exec.aws.iam.user.create(ctx, instance_name)
    assert status, result
    copy.deepcopy(result)


@pytest.mark.asyncio
async def test_list(hub, ctx, instance_name):
    status, result = await hub.exec.aws.iam.user.list(ctx)
    assert status, result
    assert instance_name in result
    copy.deepcopy(result)


@pytest.mark.asyncio
async def test_get(hub, ctx, instance_name):
    status, result = await hub.exec.aws.iam.user.get(ctx, instance_name)
    assert status, result
    assert result.name == instance_name
    assert result.tags == {ctx.acct.provider_tag_key: instance_name}
    copy.deepcopy(result)


@pytest.mark.skip("Doesn't work with localstack")
@pytest.mark.asyncio
async def test_tag(hub, ctx, instance_name):
    status, result = await hub.exec.aws.iam.user.tag(
        ctx, name=instance_name, tags={"idem_aws_test_tag": "value"}
    )
    assert status, result
    copy.deepcopy(result)


@pytest.mark.skip("Doesn't work with localstack")
@pytest.mark.asyncio
async def test_tags(hub, ctx, instance_name):
    status, result = await hub.exec.aws.iam.user.tags(ctx, instance_name)
    assert status, result
    assert result == {
        ctx.acct.provider_tag_key: instance_name,
        "idem_aws_test_tag": "value",
    }
    copy.deepcopy(result)


@pytest.mark.skip("Doesn't work with localstack")
@pytest.mark.asyncio
async def test_untag(hub, ctx, instance_name):
    status, result = await hub.exec.aws.iam.user.untag(
        ctx, instance_name, keys=["idem_aws_test_tag"]
    )
    assert status, result
    copy.deepcopy(result)

    # Verify that the tag is now gone
    status, result = await hub.exec.aws.iam.user.tags(ctx, instance_name)
    assert status, result
    assert result == {ctx.acct.provider_tag_key: instance_name}


@pytest.mark.asyncio
async def test_update(hub, ctx, instance_name):
    status, result = await hub.exec.aws.iam.user.update(
        ctx, name=instance_name, action="reload"
    )
    assert status, result
    copy.deepcopy(result)


@pytest.mark.asyncio
async def test_delete(hub, ctx, instance_name):
    status, result = await hub.exec.aws.iam.user.delete(ctx, instance_name)
    assert status, result
    copy.deepcopy(result)
