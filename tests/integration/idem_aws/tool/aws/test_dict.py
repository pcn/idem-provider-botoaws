def test_camelize(hub):
    ret = hub.tool.aws.dict.camelize(
        {
            "key": "value",
            "key_space": "value",
            "AlreadyCameled": "value",
            "skip_none_value": None,
        }
    )
    assert "Key" in ret
    assert "KeySpace" in ret
    assert "AlreadyCameled" in ret
    assert "SkipNoneValue" not in ret
    assert len(ret) == 3


def test_de_camelize(hub):
    ret = hub.tool.aws.dict.de_camelize(
        {
            "Key": "Basic test",
            "HTTPClient": "All Caps test",
            "S3Host": "Has a number",
            "RecursiveThing": {"RecursedThing": "Value"},
        }
    )
    assert "key" in ret
    assert "http_client" in ret
    assert "s3_host" in ret
    assert "recursive_thing" in ret
    assert len(ret) == 4
    assert "recursed_thing" in ret.recursive_thing


def test_flatten_tags(hub):
    ret = hub.tool.aws.dict.flatten_tags(
        {
            "outer_dict": [
                {
                    "Tags": [
                        {"key": "Foo", "value": "Bar"},
                        {"key": "Baz", "value": "Bop"},
                    ]
                }
            ]
        }
    )

    assert ret == {"outer_dict": [{"Tags": {"Foo": "Bar", "Baz": "Bop"}}]}
