import asyncio
import pytest


def func(foo, bar=None):
    return None


async def async_func(foo, bar=None):
    return None


@pytest.mark.asyncio
async def test_awaitable(hub):
    ret = await hub.tool.aws.wrap.awaitable(func, foo=1, bar=2)
    assert not asyncio.iscoroutine(ret)
    assert ret is None


@pytest.mark.asyncio
async def test_awaitable_pass(hub):
    ret = await hub.tool.aws.wrap.awaitable(async_func, foo=1, bar=2)
    assert not asyncio.iscoroutine(ret)
    assert ret is None
