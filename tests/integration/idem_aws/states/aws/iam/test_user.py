import pytest


@pytest.mark.asyncio
async def test_present(hub, ctx, instance_name):
    ret = await hub.states.aws.iam.user.present(ctx, name=instance_name)
    assert ret["name"] == instance_name
    assert ret["result"], ret["comment"]
    assert "Created" in ret["comment"]
    assert ret["changes"]["new"]


@pytest.mark.asyncio
async def test_absent(hub, ctx, instance_name):
    ret = await hub.states.aws.iam.user.absent(ctx, instance_name)
    assert ret["name"] == instance_name
    assert ret["result"], ret["comment"]
    assert "Deleted" in ret["comment"]
    assert ret["changes"]["old"]
