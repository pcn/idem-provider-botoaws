import pytest


@pytest.mark.asyncio
async def test_present(hub, ctx, instance_name):
    ret = await hub.states.aws.iam.role.present(
        ctx, name=instance_name, assume_role_policy_document="string",
    )
    assert ret["result"], ret["comment"]
    assert "Created" in ret["comment"]


@pytest.mark.asyncio
async def test_absent(hub, ctx, instance_name):
    ret = await hub.states.aws.iam.role.absent(ctx, instance_name)
    assert ret["result"], ret["comment"]
    assert "Deleted" in ret["comment"]
