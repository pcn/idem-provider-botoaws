import pytest


@pytest.mark.asyncio
async def test_present(hub, ctx, instance_name):
    ret = await hub.states.aws.dynamodb.table.present(
        ctx,
        name=instance_name,
        attribute_definitions=[{"AttributeName": "string", "AttributeType": "S"}],
        key_schema=[{"AttributeName": "string", "KeyType": "HASH"}],
        provisioned_throughput={"ReadCapacityUnits": 123, "WriteCapacityUnits": 123},
        tags=[{"Key": "string", "Value": "string"}],
    )
    assert ret["result"], ret["comment"]
    assert "Created" in ret["comment"]


@pytest.mark.asyncio
async def test_absent(hub, ctx, instance_name):
    ret = await hub.states.aws.dynamodb.table.absent(ctx, instance_name)
    assert ret["result"], ret["comment"]
    assert "Deleted" in ret["comment"]
