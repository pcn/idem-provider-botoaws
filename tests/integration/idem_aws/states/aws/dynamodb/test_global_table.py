import pytest


@pytest.mark.asyncio
async def test_present(hub, ctx, instance_name):
    ret = await hub.states.aws.dynamodb.global_table.present(ctx, name=instance_name,)
    assert ret["result"], ret["comment"]
    assert "Created" in ret["comment"]


@pytest.mark.asyncio
async def test_absent(hub, ctx, instance_name):
    ret = await hub.states.aws.dynamodb.global_table.absent(ctx, instance_name)
    assert ret["result"], ret["comment"]
    assert "Deleted" in ret["comment"]
