import pytest


@pytest.mark.asyncio
async def test_present(hub, ctx, instance_name, ipv4_cidr_block):
    ret = await hub.states.aws.ec2.vpc.present(
        ctx, name=instance_name, cidr_block=ipv4_cidr_block
    )
    assert ret["result"], ret["comment"]
    assert "Created" in ret["comment"]


@pytest.mark.asyncio
async def test_absent(hub, ctx, instance_name):
    ret = await hub.states.aws.ec2.vpc.absent(ctx, name=instance_name)
    assert ret["result"], ret["comment"]
    assert "Deleted" in ret["comment"]
