import pytest


@pytest.mark.asyncio
async def test_present(hub, ctx, instance_name, aws_ec2_vpc, ipv4_cidr_block):
    ret = await hub.states.aws.ec2.subnet.present(
        ctx, name=instance_name, cidr_block=ipv4_cidr_block, vpc=aws_ec2_vpc.name
    )
    assert ret["result"], ret["comment"]
    assert "Created" in ret["comment"]


@pytest.mark.asyncio
async def test_absent(hub, ctx, instance_name):
    ret = await hub.states.aws.ec2.subnet.absent(ctx, name=instance_name)
    assert ret["result"], ret["comment"]
    assert "Deleted" in ret["comment"]
