import pytest


@pytest.mark.asyncio
async def test_present(hub, ctx, instance_name):
    ret = await hub.states.aws.ec2.volume.present(
        ctx, name=instance_name, availability_zone="zone", size=1024
    )
    assert ret["result"], ret["comment"]
    assert "Created" in ret["comment"]


@pytest.mark.asyncio
async def test_absent(hub, ctx, instance_name):
    ret = await hub.states.aws.ec2.volume.absent(ctx, name=instance_name)
    assert ret["result"], ret["comment"]
    assert "Deleted" in ret["comment"]
