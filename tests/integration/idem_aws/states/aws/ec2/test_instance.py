import pytest


@pytest.mark.asyncio
async def test_present(hub, ctx, instance_name, aws_ec2_subnet):
    ret = await hub.states.aws.ec2.instance.present(
        ctx, name=instance_name, subnet=aws_ec2_subnet.name
    )
    assert ret["name"] == instance_name
    assert ret["result"], ret["comment"]
    assert "Created" in ret["comment"]
    assert not ret["changes"].get("old")
    assert ret["changes"]["new"]


@pytest.mark.asyncio
async def test_absent(hub, ctx, instance_name):
    ret = await hub.states.aws.ec2.instance.absent(ctx, instance_name)
    assert ret["name"] == instance_name
    assert ret["result"], ret["comment"]
    assert "Deleted" in ret["comment"]
    assert ret["changes"]["old"]
