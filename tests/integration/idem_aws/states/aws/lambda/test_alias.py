import pytest


@pytest.mark.asyncio
async def test_present(hub, ctx, instance_name, aws_lambda_function):
    ret = await hub.states.aws.λ.alias.present(
        ctx,
        name=instance_name,
        function_name=aws_lambda_function.name,
        function_version=aws_lambda_function.configuration.version,
    )
    assert ret["result"], ret["comment"]
    assert "Created" in ret["comment"]


@pytest.mark.asyncio
@pytest.mark.skip("Method not allowed")
async def test_absent(hub, ctx, instance_name, aws_lambda_function):
    ret = await hub.states.aws.λ.alias.absent(
        ctx, instance_name, function_name=aws_lambda_function.name
    )
    assert ret["result"], ret["comment"]
    assert "Deleted" in ret["comment"]
