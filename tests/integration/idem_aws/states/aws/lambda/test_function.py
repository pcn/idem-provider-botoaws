import pytest


@pytest.mark.asyncio
async def test_present(hub, ctx, instance_name, aws_iam_role, zip_file):
    ret = await hub.states.aws.λ.function.present(
        ctx,
        name=instance_name,
        runtime="python3.7",
        handler="code.main",
        role=aws_iam_role.name,
        code={"ZipFile": zip_file},
    )
    assert ret["result"], ret["comment"]
    assert "Created" in ret["comment"]


@pytest.mark.asyncio
async def test_absent(hub, ctx, instance_name):
    ret = await hub.states.aws.λ.function.absent(ctx, instance_name)
    assert ret["result"], ret["comment"]
    assert "Deleted" in ret["comment"]
