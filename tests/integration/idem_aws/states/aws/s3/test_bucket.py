import pytest


@pytest.mark.asyncio
async def test_present(hub, ctx, instance_name):
    ret = await hub.states.aws.s3.bucket.present(ctx, name=instance_name)
    assert ret["result"], ret["comment"]
    assert "Created" in ret["comment"]


@pytest.mark.asyncio
async def test_absent(hub, ctx, instance_name):
    ret = await hub.states.aws.s3.bucket.absent(ctx, instance_name)
    assert ret["result"], ret["comment"]
    assert "Deleted" in ret["comment"]
