import pytest


@pytest.mark.asyncio
async def test_present(hub, ctx, instance_name, aws_s3_bucket):
    ret = await hub.states.aws.s3.object.present(
        ctx, name=instance_name, bucket=aws_s3_bucket.name
    )
    assert ret["result"], ret["comment"]
    assert "Created" in ret["comment"]


@pytest.mark.asyncio
async def test_absent(hub, ctx, instance_name, aws_s3_bucket):
    ret = await hub.states.aws.s3.object.absent(
        ctx, instance_name, bucket=aws_s3_bucket.name
    )
    assert ret["result"], ret["comment"]
    assert "Deleted" in ret["comment"]
