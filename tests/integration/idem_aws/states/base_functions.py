import pytest


@pytest.mark.asyncio
async def test_present(hub, ctx, instance_name):
    ret = await hub.states.aws.TODO_CLIENT.TODO_SUB.present(ctx, name=instance_name)
    assert ret["name"] == instance_name
    assert ret["result"], ret["comment"]
    assert "Created" in ret["comment"]
    assert not ret["changes"]["old"]
    assert ret["changes"]["new"]


@pytest.mark.asyncio
async def test_absent(hub, ctx, instance_name):
    ret = await hub.states.aws.TODO_CLIENT.TODO_SUB.absent(ctx, instance_name)
    assert ret["name"] == instance_name
    assert ret["result"], ret["comment"]
    assert "Deleted" in ret["comment"]
    assert ret["changes"]["old"]
    assert not ret["changes"]["new"]
