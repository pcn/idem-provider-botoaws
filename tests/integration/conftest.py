import dict_tools
import mock
import pathlib
import pytest
import random
import string
import tempfile
import zipfile
from typing import Any, Dict, List


@pytest.fixture(scope="session", autouse=True)
def acct_subs() -> List[str]:
    return ["aws"]


@pytest.fixture(scope="session", autouse=True)
def acct_profile() -> str:
    return "test_development_idem_aws"


@pytest.fixture(scope="session")
def hub(hub, session_backend: str):
    hub.pop.sub.add("idem.idem")

    with mock.patch(
        "sys.argv",
        ["idem", "state", "--wrap-serial-calls", "--session-backend", session_backend],
    ):
        hub.pop.config.load(["idem", "acct"], "idem", parse_cli=True)

    yield hub


@pytest.mark.asyncio
@pytest.fixture(scope="module")
async def ctx(
    hub, acct_subs: List[str], acct_profile: str, session_backend: str
) -> Dict[str, Any]:
    ctx = dict_tools.data.NamespaceDict(run_name="test", test=False)

    # Add the profile to the account
    hub.acct.init.unlock(hub.OPT.acct.acct_file, hub.OPT.acct.acct_key)
    ctx.acct = await hub.acct.init.gather(acct_subs, acct_profile)

    yield ctx


@pytest.fixture(scope="module")
def instance_name():
    yield "test-idem-cloud-" + "".join(
        random.choice(string.ascii_lowercase + string.digits) for _ in range(20)
    )


@pytest.fixture(scope="module")
@pytest.mark.asyncio
async def aws_ec2_subnet(hub, ctx, aws_ec2_vpc, ipv4_cidr_block):
    """
    Create and cleanup an Ec2 subnet for a module that needs it
    :return: a description of an ec2 subnet
    """
    subnet_name = "test_idem_aws_subnet_" + "".join(
        random.choice(string.ascii_lowercase + string.digits) for _ in range(20)
    )
    ret = await hub.states.aws.ec2.subnet.present(
        ctx, name=subnet_name, cidr_block=ipv4_cidr_block, vpc=aws_ec2_vpc.name
    )
    assert ret["result"], ret["comment"]
    status, subnet = await hub.exec.aws.ec2.subnet.get(ctx, name=subnet_name)
    assert status, subnet.exception

    yield subnet

    ret = await hub.states.aws.ec2.subnet.absent(ctx, name=subnet_name)
    assert ret["result"], ret["comment"]


@pytest.fixture(scope="module")
@pytest.mark.asyncio
async def aws_ec2_vpc(hub, ctx, ipv4_cidr_block):
    """
    Create and cleanup an Ec2 vpc for a module that needs it
    :return: a description of an ec2 vpc
    """
    vpc_name = "test_idem_aws_vpc_" + "".join(
        random.choice(string.ascii_lowercase + string.digits) for _ in range(20)
    )
    ret = await hub.states.aws.ec2.vpc.present(
        ctx, name=vpc_name, cidr_block=ipv4_cidr_block
    )
    assert ret["result"], ret["comment"]
    status, vpc = await hub.exec.aws.ec2.vpc.get(ctx, name=vpc_name)
    assert status, vpc.exception

    yield vpc

    ret = await hub.states.aws.ec2.vpc.absent(ctx, name=vpc_name)
    assert ret["result"], ret["comment"]


@pytest.fixture(scope="module")
@pytest.mark.asyncio
async def aws_dynamodb_table(hub, ctx, aws_dynamodb_item_name):
    """
    Create and cleanup a Dynamodb table for a module that needs it
    :return: a description of an dynamodb table
    """
    table_name = "test_idem_aws_table_" + "".join(
        random.choice(string.ascii_lowercase + string.digits) for _ in range(20)
    )
    ret = await hub.states.aws.dynamodb.table.present(
        ctx,
        name=table_name,
        attribute_definitions=[
            {"AttributeName": aws_dynamodb_item_name, "AttributeType": "S"}
        ],
        key_schema=[{"AttributeName": aws_dynamodb_item_name, "KeyType": "HASH"}],
        provisioned_throughput={"ReadCapacityUnits": 123, "WriteCapacityUnits": 123},
    )
    assert ret["result"], ret["comment"]
    status, table = await hub.exec.aws.dynamodb.table.get(ctx, name=table_name)
    assert status, table.exception

    yield table

    ret = await hub.states.aws.dynamodb.table.absent(ctx, name=table_name)
    assert ret["result"], ret["comment"]


@pytest.fixture(scope="module")
def aws_dynamodb_item_name(hub, ctx) -> str:
    item_name = "test_idem_aws_item_" + "".join(
        random.choice(string.ascii_lowercase + string.digits) for _ in range(20)
    )
    yield item_name


@pytest.fixture(scope="module")
@pytest.mark.asyncio
async def aws_iam_role(hub, ctx):
    """
    Create and cleanup an IAM role for a module that needs it
    :return: a description of an iam role
    """
    role_name = "test_idem_aws_role_" + "".join(
        random.choice(string.ascii_lowercase + string.digits) for _ in range(20)
    )
    ret = await hub.states.aws.iam.role.present(
        ctx, name=role_name, assume_role_policy_document="string",
    )
    assert ret["result"], ret["comment"]
    status, role = await hub.exec.aws.iam.role.get(ctx, name=role_name)
    assert status, role.exception

    yield role

    ret = await hub.states.aws.iam.role.absent(ctx, name=role_name)
    assert ret["result"], ret["comment"]


@pytest.fixture(scope="module")
@pytest.mark.asyncio
async def aws_lambda_function(hub, ctx, zip_file, aws_iam_role):
    function_name = "test_idem_aws_function_" + "".join(
        random.choice(string.ascii_lowercase + string.digits) for _ in range(20)
    )

    ret = await hub.states.aws.λ.function.present(
        ctx,
        name=function_name,
        runtime="python3.7",
        handler="code.main",
        role=aws_iam_role.name,
        code={"ZipFile": zip_file},
    )
    assert ret["result"], ret["comment"]
    assert "Created" in ret["comment"]

    status, func = await hub.exec.aws.λ.function.get(ctx, name=function_name)
    assert status, func.exception

    yield func

    ret = await hub.states.aws.λ.function.absent(ctx, function_name)
    assert ret["result"], ret["comment"]
    assert "Deleted" in ret["comment"]


@pytest.fixture(scope="module")
@pytest.mark.asyncio
async def aws_s3_bucket(hub, ctx):
    bucket_name = "testIdemAWSBucket" + "".join(
        random.choice(string.ascii_lowercase + string.digits) for _ in range(20)
    )

    result = await hub.states.aws.s3.bucket.present(
        ctx, name=bucket_name, object_lock_enabled_for_bucket=False
    )
    assert result["result"], result["comment"]
    assert result.get("name") == bucket_name

    status, bucket = await hub.exec.aws.s3.bucket.get(ctx, bucket_name)
    assert status, bucket.exception

    yield bucket

    ret = await hub.states.aws.s3.bucket.absent(ctx, name=bucket_name)
    assert ret["result"], ret["comment"]


@pytest.fixture(scope="module")
def ipv4_cidr_block():
    """
    Generate a random Ipv4 CIDR block
    """
    num = lambda: random.randint(0, 256)
    return f"{num()}.{num()}.{num()}.0/24"


@pytest.fixture(scope="module")
def zip_file():
    """
    Create a zip file for tests that use lambda functions
    """
    with tempfile.TemporaryDirectory() as tempdir:
        path = pathlib.Path(tempdir)
        z_path = path.joinpath("code.zip")

        with zipfile.ZipFile(z_path, "w") as myzip:
            myzip.writestr("code.py", "def main():\n\treturn 0")

        with open(z_path, "rb") as fh:
            yield fh.read()
