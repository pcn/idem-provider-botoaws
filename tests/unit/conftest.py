import mock
import pytest
from dict_tools import data


@pytest.fixture
def hub(hub):
    hub.pop.sub.add("idem.idem")
    return hub


@pytest.fixture
def ctx(session_backend_lib):
    """
    Override pytest-pop's ctx fixture with one that has a mocked session
    """
    yield data.NamespaceDict(
        acct=data.NamespaceDict(
            provider_tag_key="provider_tag_key",
            session=mock.create_autospec(session_backend_lib.session.Session),
            endpoint_url="https://fake_endpoint_url.com:4566",
        )
    )
