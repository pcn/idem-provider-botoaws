def test_sub_alias(hub):
    """
    Test that the sub aliases work correctly.
    Internally, use a "λ" or "lambda_" for the sub name since "lambda" is a python keyword and causes syntax errors.
    For states it is acceptable to use "lambda" or any of the aliases since it is yaml not python
    """
    assert getattr(hub.exec.aws, "lambda") == hub.exec.aws.lambda_ == hub.exec.aws.λ
