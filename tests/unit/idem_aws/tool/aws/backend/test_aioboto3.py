from dict_tools import data
import mock
import pytest

try:
    import aioboto3.session

    HAS_LIBS = True
except ImportError:
    HAS_LIBS = False


@pytest.mark.skipif(not HAS_LIBS, reason="aioboto3 is not installed")
def test_get(hub):
    if "aioboto3" not in hub.tool.aws.backend._loaded:
        pytest.skip("aioboto3 is not loaded")

    with mock.patch.object(
        hub,
        "OPT",
        data.NamespaceDict(idem=data.NamespaceDict(session_backend="aioboto3")),
    ):
        ctx = hub.tool.aws.session.get()

    assert "endpoint_url" in ctx
    assert ctx.provider_tag_key
    assert ctx.session
    assert isinstance(ctx.session, aioboto3.session.Session)
