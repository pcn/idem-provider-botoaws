from dict_tools import data
import mock
import pytest

try:
    import localstack_client.session

    HAS_LIBS = True
except ImportError:
    HAS_LIBS = False


@pytest.mark.skipif(not HAS_LIBS, reason="localstack_client is not installed")
def test_get(hub):

    with mock.patch.object(
        hub,
        "OPT",
        data.NamespaceDict(idem=data.NamespaceDict(session_backend="localstack")),
    ):
        ctx = hub.tool.aws.session.get()

    assert "endpoint_url" in ctx
    assert ctx.provider_tag_key
    assert ctx.session
    assert isinstance(ctx.session, localstack_client.session.Session)
