import mock
import pytest


@pytest.mark.asyncio
async def test_get(hub, mock_hub, ctx):
    resource = "resource"
    mock_hub.tool.aws.resource.get = hub.tool.aws.resource.get

    async for _ in mock_hub.tool.aws.resource.get(ctx, resource):
        ctx.acct.session.resource.assert_called_once_with(
            resource, endpoint_url=ctx.acct.endpoint_url
        )
