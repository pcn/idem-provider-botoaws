from dict_tools import data
import mock


def test_get(hub):
    with mock.patch.object(
        hub, "OPT", data.NamespaceDict(idem=data.NamespaceDict(session_backend=None))
    ):
        ctx = hub.tool.aws.session.get()

    assert "endpoint_url" in ctx
    assert ctx.provider_tag_key
    assert ctx.session
