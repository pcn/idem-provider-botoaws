import pytest


@pytest.mark.asyncio
async def test_get(hub, mock_hub, ctx):
    client = "client"
    mock_hub.tool.aws.client.get = hub.tool.aws.client.get

    async for _ in mock_hub.tool.aws.client.get(ctx, client):
        ctx.acct.session.client.assert_called_once_with(
            client, endpoint_url=ctx.acct.endpoint_url
        )


@pytest.mark.asyncio
async def test_request(hub, mock_hub, ctx):
    client = "client"
    kwargs = {"key": "value", "other_key": "Value", "ignored_key": None}
    mock_hub.tool.aws.client.request = hub.tool.aws.client.request
    mock_hub.tool.aws.client.get = hub.tool.aws.client.get
    mock_hub.tool.aws.wrap.awaitable = hub.tool.aws.wrap.awaitable

    _, ret = await mock_hub.tool.aws.client.request(ctx, client, "function", **kwargs)

    ctx.acct.session.client.assert_called_once_with(
        client, endpoint_url=ctx.acct.endpoint_url
    )
    function = ctx.acct.session.client.return_value.__aenter__.return_value.function
    function.assert_called_once_with(Key="value", OtherKey="Value")
    assert function.return_value == ret
