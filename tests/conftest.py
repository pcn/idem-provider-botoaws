import importlib
import pytest


def pytest_addoption(parser):
    group = parser.getgroup("Idem AWS")
    group.addoption(
        "--session-backend",
        default="localstack",
        choices=["aioboto3", "boto3", "localstack"],
        help="Specify the boto session backend plugin to use",
    )


@pytest.fixture(scope="session", autouse=True)
def session_backend(request):
    session_backend = request.config.getoption("session_backend", "localstack")
    yield session_backend


@pytest.fixture(scope="session")
def session_backend_lib(session_backend):
    if session_backend == "localstack":
        session_backend = "localstack_client"
    backend_lib = importlib.import_module(session_backend)
    yield backend_lib
