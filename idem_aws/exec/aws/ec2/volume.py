from typing import Any, Dict, List, Tuple

__func_alias__ = {"list_": "list"}


async def create(
    hub, ctx, name: str, availability_zone: str, size: int, **kwargs
) -> Tuple[bool, Dict[str, Any]]:
    return await hub.tool.aws.client.request(
        ctx,
        client="ec2",
        func="create_volume",
        dry_run=ctx.test,
        availability_zone=availability_zone,
        size=size,
        tag_specifications=[
            {
                "ResourceType": "volume",
                "Tags": [{"Key": ctx.acct.provider_tag_key, "Value": name}],
            }
        ]
        + kwargs.get("tag_specifications", []),
        **kwargs,
    )


async def delete(hub, ctx, name: str, **kwargs) -> Tuple[bool, Dict[str, Any]]:
    _, volume = await hub.exec.aws.ec2.volume.get(ctx, name)
    return await hub.tool.aws.client.request(
        ctx, client="ec2", func="delete_volume", volume_id=volume.volume_id, **kwargs
    )


async def get(hub, ctx, name: str) -> Tuple[bool, Dict[str, Any]]:
    status, volumes = await hub.tool.aws.client.request(
        ctx,
        client="ec2",
        func="describe_volumes",
        dry_run=ctx.test,
        max_results=1,
        filters=[{"Name": f"tag:{ctx.acct.provider_tag_key}", "Values": [name]}],
    )
    if len(volumes.volumes):
        volume = volumes.volumes[0]
        volume.name = name
        return status, volume
    else:
        return False, {}


async def list_(hub, ctx, **kwargs) -> Tuple[bool, List[Dict[str, Any]]]:
    return await hub.tool.aws.client.request(
        ctx,
        client="ec2",
        func="describe_volumes",
        filters=[{"Name": "tag-key", "Values": [ctx.acct.provider_tag_key]}],
        dry_run=ctx.test,
        **kwargs,
    )


async def _tag(hub, ctx, volume_id: str, tags: Dict[str, str], **kwargs):
    return await hub.tool.aws.resource.request(
        ctx,
        "ec2",
        "Volume",
        "create_tags",
        resource_id=volume_id,
        dry_run=ctx.test,
        tags=[{"Key": k, "Value": v} for k, v in tags.items()],
        **kwargs,
    )


async def tag(hub, ctx, name: str, tags: Dict[str, str]):
    _, volume = await hub.exec.aws.ec2.volume.get(ctx, name)
    status, ret = await _tag(hub, ctx, volume_id=volume.volume_id, tags=tags)
    if status is False:
        return status, ret

    return True, tags


async def tags(hub, ctx, name: str, **kwargs):
    _, volume = await hub.exec.aws.ec2.volume.get(ctx, name)
    return await hub.tool.aws.client.request(
        ctx,
        "ec2",
        "describe_tags",
        dry_run=ctx.test,
        filters=[
            {"Name": "resource-type", "Values": ["volume"]},
            {"Name": "resource-id", "Values": [volume.volume_id]},
        ],
        **kwargs,
    )


async def untag(hub, ctx, name: str, keys: List[str]) -> Tuple[bool, Any]:
    _, volume = await hub.exec.aws.ec2.volume.get(ctx, name)
    status, tags_ = await hub.exec.aws.ec2.volume.tags(ctx, name)
    if not status:
        return status, tags_

    result = {}
    for key in keys:
        if key in tags_:
            status, result = await hub.tool.aws.resource.request(
                ctx,
                "ec2",
                "Tag",
                "delete",
                resource_id=volume.volume_id,
                resource_args=(key, tags_[key]),
                dry_run=ctx.test,
            )
            if not status:
                return status, result

    return status, result


async def update(
    hub, ctx, action: str, name: str, **kwargs
) -> Tuple[bool, Dict[str, Any]]:
    """
    :param hub:
    :param ctx:
    :param name: The volume name
    :param action: The resource action to perform.
        Available actions are:
            - associate_dhcp_options
            - attach_to_instance
            - create_snapshot
            - detach_from_instance
            - enable_io
            - modify_attribute
    :param kwargs: keyword arguments to pass to the resource action
    """
    _, volume = await hub.exec.aws.ec2.volume.get(ctx, name)
    return await hub.tool.aws.resource.request(
        ctx, "ec2", "Volume", action, resource_id=volume.volume_id, **kwargs,
    )
