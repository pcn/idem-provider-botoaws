# -*- coding: utf-8 -*-
"""
EC2 Subnet
"""
from typing import Any, Dict, List, Tuple

__func_alias__ = {"list_": "list"}


async def list_(hub, ctx):
    return await hub.tool.aws.client.request(
        ctx, client="ec2", func="describe_subnets", dry_run=ctx.test
    )


async def create(hub, ctx, name: str, cidr_block: str, vpc: str, **kwargs):
    _, vpc = await hub.exec.aws.ec2.vpc.get(ctx, vpc)
    status, ret = await hub.tool.aws.client.request(
        ctx,
        client="ec2",
        func="create_subnet",
        cidr_block=cidr_block,
        vpc_id=vpc.vpc_id,
        dry_run=ctx.test,
        **kwargs,
    )
    if not status:
        return status, ret
    await _tag(hub, ctx, ret.subnet.subnet_id, tags={ctx.acct.provider_tag_key: name})
    return status, ret


async def delete(hub, ctx, name: str, **kwargs):
    _, subnet = await hub.exec.aws.ec2.subnet.get(ctx, name)
    return await hub.tool.aws.client.request(
        ctx,
        client="ec2",
        func="delete_subnet",
        subnet_id=subnet.subnet_id,
        dry_run=ctx.test,
        **kwargs,
    )


async def get(hub, ctx, name: str):
    status, subnets = await hub.tool.aws.client.request(
        ctx,
        client="ec2",
        func="describe_subnets",
        dry_run=ctx.test,
        filters=[{"Name": f"tag:{ctx.acct.provider_tag_key}", "Values": [name]}],
    )

    if not status or not len(subnets.subnets):
        return False, {}

    subnet = subnets.subnets[0]
    subnet.name = name
    return True, subnet


async def _tag(hub, ctx, subnet_id: str, tags: Dict[str, str], **kwargs):
    return await hub.tool.aws.resource.request(
        ctx,
        "ec2",
        "Subnet",
        "create_tags",
        resource_id=subnet_id,
        dry_run=ctx.test,
        tags=[{"Key": k, "Value": v} for k, v in tags.items()],
        **kwargs,
    )


async def tag(hub, ctx, name: str, tags: Dict[str, str]):
    _, subnet = await hub.exec.aws.ec2.subnet.get(ctx, name)
    status, ret = await _tag(hub, ctx, subnet_id=subnet.subnet_id, tags=tags,)
    if status is False:
        return status, ret

    return True, tags


async def tags(hub, ctx, name: str, **kwargs):
    _, subnet = await hub.exec.aws.ec2.subnet.get(ctx, name)
    return await hub.tool.aws.client.request(
        ctx,
        "ec2",
        "describe_tags",
        dry_run=ctx.test,
        filters=[
            {"Name": "resource-type", "Values": ["subnet"]},
            {"Name": "resource-id", "Values": [subnet.subnet_id]},
        ],
        **kwargs,
    )


async def untag(hub, ctx, name: str, keys: List[str]) -> Tuple[bool, Any]:
    _, subnet = await hub.exec.aws.ec2.subnet.get(ctx, name)
    status, tags_ = await hub.exec.aws.ec2.subnet.tags(ctx, name)
    if not status:
        return status, tags_

    result = {}
    for key in keys:
        if key in tags_:
            status, result = await hub.tool.aws.resource.request(
                ctx,
                "ec2",
                "Tag",
                "delete",
                resource_id=subnet.subnet_id,
                resource_args=(key, tags_[key]),
                dry_run=ctx.test,
            )
            if not status:
                return status, result

    return status, result


async def update(hub, ctx, name: str, **kwargs) -> Tuple[bool, Dict[str, Any]]:
    _, subnet = await hub.exec.aws.ec2.subnet.get(ctx, name)
    return await hub.tool.aws.client.request(
        ctx,
        client="ec2",
        func="modify_subnet_attribute",
        subnet_id=subnet.subnet_id,
        **kwargs,
    )
