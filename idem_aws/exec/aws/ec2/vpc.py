# -*- coding: utf-8 -*-
"""
EC2 VPC
"""
from typing import Any, Dict, List, Tuple

__func_alias__ = {"list_": "list"}


async def list_(hub, ctx):
    return await hub.tool.aws.client.request(
        ctx, client="ec2", func="describe_vpcs", dry_run=ctx.test
    )


async def create(hub, ctx, name: str, cidr_block: str, **kwargs):
    status, ret = await hub.tool.aws.client.request(
        ctx,
        client="ec2",
        func="create_vpc",
        cidr_block=cidr_block,
        dry_run=ctx.test,
        **kwargs,
    )
    if not status:
        return status, ret
    await _tag(hub, ctx, ret.vpc.vpc_id, tags={ctx.acct.provider_tag_key: name})
    return status, ret


async def delete(hub, ctx, name: str, **kwargs):
    _, vpc = await hub.exec.aws.ec2.vpc.get(ctx, name)
    return await hub.tool.aws.client.request(
        ctx,
        client="ec2",
        func="delete_vpc",
        vpc_id=vpc.vpc_id,
        dry_run=ctx.test,
        **kwargs,
    )


async def get(hub, ctx, name: str):
    status, vpcs = await hub.tool.aws.client.request(
        ctx,
        client="ec2",
        func="describe_vpcs",
        dry_run=ctx.test,
        filters=[{"Name": f"tag:{ctx.acct.provider_tag_key}", "Values": [name]}],
    )

    if not status or not len(vpcs.vpcs):
        return False, {}

    vpc = vpcs.vpcs[0]
    vpc.name = name
    return True, vpc


async def _tag(hub, ctx, vpc_id: str, tags: Dict[str, str], **kwargs):
    return await hub.tool.aws.resource.request(
        ctx,
        "ec2",
        "Vpc",
        "create_tags",
        resource_id=vpc_id,
        dry_run=ctx.test,
        tags=[{"Key": k, "Value": v} for k, v in tags.items()],
        **kwargs,
    )


async def tag(hub, ctx, name: str, tags: Dict[str, str]):
    _, vpc = await hub.exec.aws.ec2.vpc.get(ctx, name)
    status, ret = await _tag(hub, ctx, vpc_id=vpc.vpc_id, tags=tags,)
    if status is False:
        return status, ret

    return True, tags


async def tags(hub, ctx, name: str, **kwargs):
    _, vpc = await hub.exec.aws.ec2.vpc.get(ctx, name)
    return await hub.tool.aws.client.request(
        ctx,
        "ec2",
        "describe_tags",
        dry_run=ctx.test,
        filters=[
            {"Name": "resource-type", "Values": ["vpc"]},
            {"Name": "resource-id", "Values": [vpc.vpc_id]},
        ],
        **kwargs,
    )


async def untag(hub, ctx, name: str, keys: List[str]) -> Tuple[bool, Any]:
    _, vpc = await hub.exec.aws.ec2.vpc.get(ctx, name)
    status, tags_ = await hub.exec.aws.ec2.vpc.tags(ctx, name)
    if not status:
        return status, tags_

    result = {}
    for key in keys:
        if key in tags_:
            status, result = await hub.tool.aws.resource.request(
                ctx,
                "ec2",
                "Tag",
                "delete",
                resource_id=vpc.vpc_id,
                resource_args=(key, tags_[key]),
                dry_run=ctx.test,
            )
            if not status:
                return status, result

    return status, result


async def update(
    hub, ctx, action: str, name: str, **kwargs
) -> Tuple[bool, Dict[str, Any]]:
    """
    :param hub:
    :param ctx:
    :param name: The vpc name
    :param action: The resource action to perform.
        At the time of writing, the available actions are:
            - associate_dhcp_options
            - attach_classic_link_instance
            - attach_internet_gateway
            - create_network_acl
            - create_route_table
            - create_security_group
            - create_subnet
            - describe_attribute
            - detach_classic_link_instance
            - detach_internet_gateway
            - disable_classic_link
            - enable_classic_link
            - get_available_subresources
            - modify_attribute
            - request_vpc_peering_connection
    :param kwargs: keyword arguments to pass to the resource action
    """
    _, vpc = await hub.exec.aws.ec2.vpc.get(ctx, name)
    return await hub.tool.aws.resource.request(
        ctx, "ec2", "Vpc", action, resource_id=vpc.vpc_id, dry_run=ctx.test, **kwargs
    )
