# -*- coding: utf-8 -*-
"""
EC2 Tag
"""

import functools
from dict_tools import data

__func_alias__ = {"list_": "list"}


async def list_(hub, ctx, **kwargs):
    ec2client = ctx["acct"]["session"].client("ec2")
    # TODO: filters
    # TODO: pagination
    pfunc = functools.partial(ec2client.describe_tags)  # XXX add filters capability
    tags = hub.exec.aws.init.yield_paginated(
        pfunc,
        'Tags',
        'NextToken',
        'NextToken')
    return [tag async for tag in tags]


async def get(hub, ctx, resource_id, key, value):
    """
    Get the tag for a resource based on providing the
    resource_id, the key and the value

    This is most useful if it is used to modify the
    tag after it's gotten.  Otherwise this is not very useful.
    """
    ec2res = ctx["acct"]["session"].resource("ec2")
    ret = ec2res.Tag(resource_id, key, value)
    ret.load()
    hub.log.warning(f"Ret is {dir(ret)}")
    return [{"resource_id": ret.resource_id, "key": ret.key, "value": ret.value}]
